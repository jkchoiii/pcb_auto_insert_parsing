import sys
from typing import List, Any, Union
from time import sleep
from PyQt5 import uic
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
# UI파일 연결
# 단, UI파일은 Python 코드 파일과 같은 디렉토리에 위치해야한다.

import pandas as pd
from pandas import DataFrame, Series
from pandas.io.parsers import TextFileReader

# import pandas as pd

form_class = uic.loadUiType("JKSolution.ui")[0]


# 화면을 띄우는데 사용되는 Class 선언
class WindowClass(QMainWindow, form_class):
    bom_ready: bool
    csv_ready: bool
    is_running: bool

    def __init__(self):
        super().__init__()
        self.setupUi(self)

        self.obj = Processor()

        self.btn_bom.clicked.connect(self.OpenExcelFile)
        self.btn_csv.clicked.connect(self.OpenCSVFile)
        self.btn_run.clicked.connect(self.Run)

        self.bom_path.textChanged.connect(self.SetBOMPath)
        self.csv_path.textChanged.connect(self.SetCSVPath)
        self.run_path.textChanged.connect(self.SetRunPath)

        self.bom_ready = False
        self.csv_ready = False
        self.is_running = False
        print('')


    def SetBOMPath(self, fname):
        self.bom_path.setText(fname)
        self.bom_path_str = fname

    def SetCSVPath(self, fname):
        self.csv_path.setText(fname)
        self.csv_path_str = fname

    def SetRunPath(self, fname):
        self.run_path.setText(fname)

    def OpenExcelFile(self):
        if not self.is_running:
            self.bom_ready = False

            fname, ok = QFileDialog.getOpenFileName(self, filter="xlsx Files(*.xlsx)")
            if ok:
                self.SetBOMPath(fname)  # ui update
                #####
                self.obj.SetSrcPath(fname)
                self.obj.SrcProcessor()
                self.bom_ready = True
                print('bom ready')
            else:
                print('no selected')

    def OpenCSVFile(self):
        if not self.is_running:
            self.csv_ready = False

            fname, ok = QFileDialog.getOpenFileName(self, filter="csv Files(*.csv)")
            if ok:
                self.SetCSVPath(fname)
                ####
                self.obj.SetDstPath(fname)
                self.obj.DstProcessor()
                self.csv_ready = True
                print('csv ready')
            else:
                print('no selected')

    def Run(self):

        if not self.is_running:
            if self.bom_ready and self.csv_ready:
                self.is_running = True
                self.btn_run.setDisabled(True)
                self.btn_bom.setDisabled(True)
                self.btn_csv.setDisabled(True)

                # self.SetRunPath(self.csv_path)
                file_name = self.csv_path.text()
                new_fn = ''
                path_splited = file_name.split('/')
                for idx in range(len(path_splited) - 1):
                    new_fn = new_fn + '/' + path_splited[idx]
                new_fn = new_fn[1:] + '/JKSln_' + path_splited[-1][:-4] + '.csv'

                print('')
                division_line = ''
                for i in range(len(self.bom_path_str)):
                    division_line += '='
                print(division_line)
                print('source bom: ' + self.bom_path_str)
                print('source csv: ' + self.csv_path_str)
                print('target: ' + new_fn)
                print(division_line)
                print('')


                self.SetRunPath(new_fn)  # ui update
                self.obj.Process()

                self.obj.WriteFile(new_fn)

                self.btn_run.setDisabled(False)
                self.btn_bom.setDisabled(False)
                self.btn_csv.setDisabled(False)
                self.is_running = False
            else:
                print('set bom and csv first')


class Processor:
    #dst_data: Union[Union[TextFileReader, Series, DataFrame, None], Any]
    fill_data: List[str]
    empty_ref: list
    nan_idx: list
    start_row: int
    ref: object
    values: object
    src_path = str
    dst_path = str



    def WriteFile(self, path):
        self.dst_data.loc[self.dst_start_row:, 1] = self.fill_data
        #self.dst_data.to_csv('C:\\Data\\JK.csv', sep=',', na_rep='NaN')
        self.dst_data.to_csv(path, sep=',', na_rep='NaN', header=False, index=False)

    def isnan(self, val):
        return isinstance(val, float) and val != val

    def Process(self):
        print('generating filled file')

        prev_progress = 0
        progress_init = False
        total_length = len(self.empty_ref)
        for i in range(total_length):
            cur_progress = 100.0*i/float(total_length)
            if cur_progress - prev_progress >= 0.1:
                prev_progress = cur_progress
                msg = str(round(prev_progress, 1))
                print(msg + '%', end="\r")
                #print(msg + '%')
                #sys.stdout.write("\033[F")

            for j, ref_val in enumerate(self.ref):
                if self.isnan(ref_val):
                    continue

                ref_val = ref_val.replace('\n', '')
                #if isinstance(ref_val, float) and ref_val != ref_val:
                if self.isnan(ref_val):
                    continue

                ref_token = ref_val.split(',')

                for token_idx, token in enumerate(ref_token):
                    if token == self.empty_ref[self.empty_ref.index[i]]:
                        access_idx = self.values.index[j]
                        self.fill_data[i] = str(self.values[access_idx])

        print('done  ')


    def SetSrcPath(self, fname):
        self.src_path = fname

    def SetDstPath(self, fname):
        self.dst_path = fname

    def FindStartIndex(self, data):
        start_row = 0
        flag = False
        for i in data[0]:
            if i == 'Item':
                flag = True
                start_row += 1
                continue

            if flag and isinstance(i, int):
                break
            start_row += 1

        return start_row

    def SrcProcessor(self):
        #print('source bom: ' + self.src_path)

        src_data = pd.read_excel(self.src_path, header=None, index_col=None)
        self.start_row = self.FindStartIndex(src_data)
        self.values = src_data.iloc[self.start_row:, 1]
        self.ref = src_data.iloc[self.start_row:, 2]

    def FindNanIndex(self):
        start_row = 0
        flag = False
        for i in self.dst_data[0]:
            if i == 'Name':
                flag = True
                start_row += 1
                continue

            if flag:
                if not (isinstance(i, float) and i != i):
                    break
            start_row += 1

        return start_row, self.dst_data.iloc[start_row:, [1]].isna().index.to_list()

    def FindEmptyRef(self):
        empty_ref = self.dst_data.loc[:, 0]
        return empty_ref[self.nan_idx]

    def DstProcessor(self):
        #print('source csv: ' + self.dst_path)
        self.dst_data = pd.read_csv(self.dst_path, header=None, index_col=None)
        self.dst_start_row, self.nan_idx = self.FindNanIndex()
        self.empty_ref = self.FindEmptyRef()
        self.fill_data = [str] * len(self.empty_ref)

'''
class Worker(QThread):
    finished = pyqtSignal()

    def run(self):
'''




if __name__ == "__main__":
    # QApplication : 프로그램을 실행시켜주는 클래스
    app = QApplication(sys.argv)
    # WindowClass의 인스턴스 생성
    myWindow = WindowClass()

    # 프로그램 화면을 보여주는 코드
    myWindow.show()

    # 프로그램을 이벤트루프로 진입시키는(프로그램을 작동시키는) 코드
    app.exec_()
